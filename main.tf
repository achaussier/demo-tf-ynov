terraform {
  required_providers {
  }
  required_version = "1.3.7"
}

locals {
  username = title(var.username)
}
