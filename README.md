# Scaleway ES

## Usage

1. install `pre-commit`

    ```shell
    pip install pre-commit
    ```

2. enable `pre-commit`

    ```shell
    pre-commit install
    ```

## Module documentation

<!-- BEGIN_TF_DOCS -->
<!-- END_TF_DOCS -->
